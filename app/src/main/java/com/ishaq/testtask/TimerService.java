package com.ishaq.testtask;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;


public class TimerService extends Service {
    public static boolean isRunning=false;
    private NotificationCompat.Builder builder;
    private NotificationManagerCompat notificationManagerCompat;
    private static final int NOTIFICATION_ID=244;

    public TimerService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        String CHANNEL_ID = "my_channel_01";
        builder=new NotificationCompat.Builder(this,CHANNEL_ID);
        builder.setAutoCancel(false);
        builder.setContentText("Time Left: 5:00");
        builder.setContentTitle("You are on the break");
        builder.setSmallIcon(R.drawable.ic_launcher_foreground);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);
        }

        startForeground(NOTIFICATION_ID, builder.build());

        final NotificationManagerCompat nm=NotificationManagerCompat.from(this);
        //nm.notify(NOTIFICATION_ID,builder.build());
        CountDownTimer timer=new CountDownTimer(1000*60*5,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                String timeLeft=getTimeLeft(millisUntilFinished);
                builder.setContentText(timeLeft);
                nm.notify(NOTIFICATION_ID,builder.build());
            }
            @Override
            public void onFinish() {
                nm.cancel(NOTIFICATION_ID);
                onDestroy();
            }
        };
        timer.start();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        isRunning=true;
        return super.onStartCommand(intent, flags, startId);
    }

    private String getTimeLeft(long elapsedMillis) {
        long different=elapsedMillis;
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;
        long elapsedSeconds = different / secondsInMilli;
        StringBuilder builder=new StringBuilder("Time Left: ");
        builder.append(elapsedMinutes);
        builder.append(":");
        builder.append(elapsedSeconds);

        return builder.toString();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning=false;
        stopSelf();
    }
}
