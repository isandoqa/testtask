package com.ishaq.testtask;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(!TimerService.isRunning){
            if(Build.VERSION.SDK_INT>=26){
                startForegroundService(new Intent(this, TimerService.class));
            }else{
                startService(new Intent(this, TimerService.class));
            }

        }
    }
}
